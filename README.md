## Dockerized MongoDB

![MongoDB Logo](https://bitbucket.org/opscrew/docker-mongodb/avatar/128) Provide a **Docker** Image with **MongoDB** service preinstalled

`docker` `MongoDB` `docker-mongodb` `dockerized-service` `noSQL`

- - - -

### Docker Image
[![Standard Version](https://img.shields.io/badge/release-standard%20version-brightgreen.svg)](https://github.com/conventional-changelog/standard-version) [![releaseQ versionQ](https://images.microbadger.com/badges/version/tbaltrushaitis/docker-mongodb.svg)](https://microbadger.com/images/tbaltrushaitis/docker-mongodb "latest") [![badge idea](https://images.microbadger.com/badges/image/tbaltrushaitis/docker-mongodb.svg)](https://microbadger.com/images/tbaltrushaitis/docker-mongodb "image badge") [![Commit Badge](https://images.microbadger.com/badges/commit/tbaltrushaitis/docker-mongodb.svg)](https://microbadger.com/images/tbaltrushaitis/docker-mongodb "Commit badge")

--------

### Service Dependencies

 Package | Version  | Tag 
:--------|:--------:|:---:
Linux   | 4.4.0-45 | generic
MongoDB | v3.4.1 | org


### Get the MongoDB Image from Docker Hub

```
$ docker pull tbaltrushaitis/docker-mongodb
```

--------

#### Run one or more MongoDB instances as daemon process(es).

##### Basic way


**Usage:**
> $ docker create --name <container name> -dt <image-name>[:<image-tag>]

```
$ docker create                                                         \
    --name mongo_instance_001                                           \
    -p 28001:27017                                                      \
    -v /storage/docker-assets/mongo_instance_001/:/dockerdata/mongodb/  \
    -it docker-mongodb_341_100:custom
```

**Usage:**
> docker run --name <name for container> -d <user-name>/<repository>

```
$ docker run -p 28017:27017 --name mongo_instance_001 -d tbaltrushaitis/docker-mongodb
```

#### Dockerized MongoDB, lean and mean!

**Usage:**
> $ docker run --name <container name> -d <user-name>/<repository> --noprealloc --smallfiles

```
$ docker run -p 28018:27017 --name mongo_instance_002 -d tbaltrushaitis/docker-mongodb --noprealloc --smallfiles
```

##### Checking out the logs of a MongoDB container

**Usage:**
> docker logs <name for container>

```
$ docker logs mongo_instance_001
$ docker logs 9355bfdb6c4e
```

##### Playing with MongoDB

**Usage:**
> $ mongo --port <port you get from `docker ps`>

```
$ mongo --port 27017
```

##### If using docker-machine

**Usage:**
> $ mongo --port <port you get from `docker ps`>  --host <ip address from `docker-machine ip VM_NAME`>

```
$ mongo --port 27017 --host 192.168.59.103
```

------------------

> **Tip:** If you need run two containers on the same engine, then you need to map the exposed port to two different ports on the host

##### Start two containers and map the ports

```
$ docker run -p 28001:27017 --name mongo_instance_001 -d tbaltrushaitis/docker-mongodb
$ docker run -p 28002:27017 --name mongo_instance_002 -d tbaltrushaitis/docker-mongodb
```


##### Now you can connect to each MongoDB instance on the two ports

```
$ mongo --port 28001
$ mongo --port 28002
```

------------------

### More Info

 - [Linking containers](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks.md)
 - [Cross-host linking containers](https://docs.docker.com/engine/admin/ambassador_pattern_linking.md)
 - [Creating an Automated Build](https://docs.docker.com/docker-hub/builds/)
