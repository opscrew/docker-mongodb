#!/bin/bash

clear

source .env;

D="$(date +"%Y%m%d")"
DATE="$(date +"%Y%m%d%H%M%S")"
DATETIME="$(date "+%Y-%m-%d")_$(date "+%H-%M-%S")"
BUILD_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

APP_VER=$(echo ${APP_VERSION} | tr -d ".")
MONGO_VER=$(echo ${MONGO_VERSION} | tr -d ".")
APP_IMAGE="${HUB_USER}/${HUB_REPO}:${SVC_VERSION}"
SRV_IMAGE="${HUB_REPO}_${SVC_VER}_${APP_VER}"
SRV_DOCKERIZED="${APP_NAME}_${APP_VER}"

# Image can be overidden with env vars.
#DOCKER_IMAGE="${APP_HUB_USER}/${APP_HUB_REPO}:${MONGO_VERSION}"
HUB_IMAGE="${HUB_USER}/${HUB_REPO}"

# Get the Repo URL and latest commit.
# VCS_URL="https://bitbucket.org/tbaltrushaitis/docker-mongodb"
VCS_URL="$(git config --get remote.origin.url)"
VCS_REF="$(git rev-parse --short HEAD)"

# Get the version number from the code
CODE_VERSION=$(cat VERSION)

# Add the commit ref for development builds. Mark as dirty if the working directory isn't clean
DOCKER_TAG=${CODE_VERSION}-${VCS_REF}

# Initial BUILD image is a service image to run containers from
BUILD_IMAGE=${SRV_IMAGE}

printf "\n------------------------------  ${DATE}  ------------------------------\n";

function dockerBuild () {
    printf "\tdockerBuild image param: \t [$1]\n";
    Image=$1;
    if [ "$1" == "" ]; then
        Image=${BUILD_IMAGE}
    fi
    printf "\tBUILD IMAGE = \t [$Image:${DOCKER_TAG}]\n\n";

    # Build Docker image
    BUILD_IMAGE_ID=$(docker -D build        \
      --quiet                               \
      --pull                                \
      --disable-content-trust=true          \
      --build-arg BUILD_DATE=${BUILD_DATE}  \
      --build-arg VERSION=${CODE_VERSION}   \
      --build-arg VCS_URL=${VCS_URL}        \
      --build-arg VCS_REF=${VCS_REF}        \
      -t ${Image}:${DOCKER_TAG}             \
      -f Dockerfile                         \
      .)

}

#      -v "/storage/docker-assets/${Image}:${MONGO_DATA_DIR}"  \
  # --rm=true                             \
#-t ${DOCKER_IMAGE}:${DOCKER_TAG}  \

function dockerPush () {
    # Push to DockerHub
    # docker push ${DOCKER_IMAGE}:${DOCKER_TAG}
    docker push ${HUB_IMAGE}:${DOCKER_TAG}
}

function dockerPushLatest () {
    # Tag image as latest
    docker tag ${HUB_IMAGE}:${DOCKER_TAG} ${HUB_IMAGE}:latest
    # Push to DockerHub
    docker push ${HUB_IMAGE}:latest
}

function dockerCreate () {
    CONTAINER_ID=$(docker -D create                                     \
        --name="${SRV_DOCKERIZED}"                                      \
        --cap-add=ALL                                                   \
        -p ${SVC_PORT_EXPOSE}:${SVC_PORT_LISTEN}                        \
        -v /storage/docker-assets/${SRV_DOCKERIZED}:${MONGO_DATA_DIR}   \
        -t ${BUILD_IMAGE}                                               \
        --dbpath ${MONGO_DATA_DIR})

    # docker create -p 28001:27017 --name mongo_instance_001 -v /storage/docker-assets/mongo_instance_001/:/dockerdata/mongodb/ -it docker-mongodb_341_100:custom

    docker ps -a
}

function dockerStart () {
    CONTAINER_ID=${CONTAINER_ID:-$(docker ps -alsq)}
    docker -D start ${CONTAINER_ID}
}


function dockerRun () {
    docker -D run                                                           \
        --name="${SRV_DOCKERIZED}"                                          \
        --cap-add=ALL                                                       \
        -p ${SVC_PORT_EXPOSE}:${SVC_PORT_LISTEN}                            \
        -v /storage/docker-assets/${SRV_DOCKERIZED}:${MONGO_DATA_DIR}:rw    \
        -dt ${HUB_IMAGE}:${DOCKER_TAG}                                   \
        --dbpath ${MONGO_DATA_DIR}

    docker ps -a
}
        # -dt ${SRV_IMAGE}  \

function printImageInfo () {
    Image=$1;
    printf "\n\n\n\tDOCKER IMAGE = [$1]\n\n";

    docker inspect -f '
        ##
        ## Docker Image Metadata
        ##

        - Image ID: {{ "\t" }} {{ .Id }}
        - Created: {{ "\t" }} {{ .Created }}
        - Arch: {{ "\t" }} {{ .Os }}/{{ .Architecture }}
        - Domainname: {{ "\t" }} {{ .Config.Domainname }}
        - Hostname: {{ "\t" }} {{ .Config.Hostname }}
        - Labels:{{range $e,$v := .Config.Labels}} {{ "\n\t\t" }} LABEL {{ $e }}{{ "=" }}{{ $v }} {{end}}
        - Environment:{{range $e,$v := .Config.Env}} {{ "\n\t\t" }} ENV {{ $v }} {{end}}
        {{if .Config.OnBuild}}ONBUILD {{json .Config.OnBuild}} {{end}}
        {{range $e,$v := .Config.Volumes}}VOLUME {{json $e}} {{end}}
        {{if .Config.User}}USER {{json .Config.User}} {{end}}
        {{if .Config.WorkingDir}}WORKDIR {{.Config.WorkingDir}} {{end}}
        {{range $e,$v := .Config.ExposedPorts}}EXPOSE {{json $e}} {{end}}
        {{if .Config.Entrypoint }}ENTRYPOINT {{ json .Config.Entrypoint}} {{ end }}
        {{if .Config.Cmd}}CMD {{json .Config.Cmd}} {{end}}
        {{with .Config}}{{ "\n\n" }}FULL_CONFIG: {{json .}} {{end}}
        ' "${Image}"
}


#docker -D run --name="${SVC_DOCKERIZED}" -p ${SVC_PORT_EXPOSE}:${SVC_PORT_LISTEN} -v "/storage/docker-assets/${SRV_DOCKERIZED}:/data/mongodb" -dt ${SVC_IMAGE}  --dbpath ${MONGO_DATA_DIR}

function log () {
    printf "\n\n\n";
    printf "APP_NAME  \t [${APP_NAME}]\n";
    printf "APP_TITLE \t [${APP_TITLE}]\n";
    printf "APP_VERSION \t [${APP_VERSION}]\n";
    printf "APP_VER \t [${APP_VER}]\n";
    printf "MONGO_VERSION \t [${MONGO_VERSION}]\n";
    printf "MONGO_VER \t [${MONGO_VER}]\n";
    printf "GIT_COMMIT \t [${GIT_COMMIT}]\n";
    printf "CODE_VERSION \t [${CODE_VERSION}]\n";
    printf "APP_IMAGE \t [${APP_IMAGE}]\n";
    printf "SRV_IMAGE \t [${SRV_IMAGE}]\n";
    printf "SRV_DOCKERIZED \t [${SRV_DOCKERIZED}]\n";
    printf "BUILD_IMAGE \t [${BUILD_IMAGE}]\n";
    printf "BUILD_IMAGE_ID \t [${BUILD_IMAGE_ID}]\n";
    printf "HUB_IMAGE \t [${HUB_IMAGE}]\n";
    printf "DOCKER_TAG  \t [${DOCKER_TAG}]\n";
    printf "CONTAINER_ID  \t [${CONTAINER_ID}]\n";
    printf "BUILD_DATE  \t [${BUILD_DATE}]\n";
    printf "VCS_URL  \t [${VCS_URL}]\n";
    printf "VCS_REF  \t [${VCS_REF}]\n";
    printf "\n\n\n";
    # printf "BASH_SOURCE \t [${BASH_SOURCE}]\n";
    # printf "BASH_VERSION \t [${BASH_VERSION}]\n";
}

function usage () {
    printf "\n\n";
    printf "Usage:\t$0 {i | log}\n";
    printf "Usage:\t$0 {clean:images | clean:containers | clean:volumes | cleanup}\n";
    printf "Usage:\t$0 {image:srv | image:hub | image:hub:push}\n";
    printf "Usage:\t$0 {container:create | container:start | container:run}\n";
    printf "\n\n";
}

function i () {
    # printImageInfo "${SRV_IMAGE}";
    # printImageInfo "${DOCKER_IMAGE}:${DOCKER_TAG}";
    printImageInfo "${BUILD_IMAGE}:${DOCKER_TAG}";
}

function cleanImages () {
    # cleanup images
    docker rmi $(docker images | grep '<none>' | awk '{print $3}')
    docker images
}

function cleanContainers () {
    # clean up exited docker containers
    docker rm -v $(docker ps -aq -f status=exited)
    docker ps -a
}

function cleanVolumes () {
    # Remove “dangling” volumes - volumes that are no longer referenced by a container
    docker volume rm $(docker volume ls -q -f dangling=true)
    docker volume ls
}

function cleanup () {
    # Clean up ALL
    cleanContainers
    cleanImages
    cleanVolumes
}

printf "\n\t------------------------- $1 ------------------------- \n";

case "$1" in

    "clean:images")
        cleanImages
    ;;
    "clean:containers")
        cleanContainers
    ;;
    "clean:volumes")
        cleanVolumes
    ;;
    "cleanup")
        cleanup
    ;;

    "image:srv")
        BUILD_IMAGE="${SRV_IMAGE}"
        printf "\tBUILD_IMAGE \t ${SRV_IMAGE}:${DOCKER_TAG}\n";
        dockerBuild "${SRV_IMAGE}"
        i
        log
    ;;

    "image:hub")
        # BUILD_IMAGE="${HUB_IMAGE}"
        printf "\tBUILD_IMAGE \t [${HUB_IMAGE}:${DOCKER_TAG}]\n";
        dockerBuild "${HUB_IMAGE}"
        log
    ;;

    "container:create")
        BUILD_IMAGE="${SRV_IMAGE}"
        printf "\tBUILD_IMAGE \t ${BUILD_IMAGE}\n";
        dockerCreate
        log
    ;;
    "container:start")
        BUILD_IMAGE="${SRV_IMAGE}"
        printf "\tBUILD_IMAGE \t ${BUILD_IMAGE}\n";
        dockerStart
        log
    ;;
    "container:run")
        BUILD_IMAGE="${SRV_IMAGE}"
        printf "\tBUILD_IMAGE \t ${BUILD_IMAGE}\n";
        dockerRun
        log
    ;;

    "image:hub:push")
        BUILD_IMAGE="${HUB_IMAGE}"
        printf "\tBUILD_IMAGE \t ${BUILD_IMAGE}:${DOCKER_TAG}\n";
        dockerPush
        log
    ;;

    "latest")
        dockerPushLatest
        log
    ;;
    "run")
        dockerRun
        docker ps -a
    ;;
    "git")
        # git_tasks
    ;;
    "log")
        log
    ;;
    "i")
        i
    ;;
    "usage")
        usage
    ;;
    *)
        usage
        log
        exit 1
        ;;
esac

# exit $RETVAL

