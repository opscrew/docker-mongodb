# Docker Image [![](https://images.microbadger.com/badges/image/tbaltrushaitis/docker-mongodb.svg)](https://microbadger.com/images/tbaltrushaitis/docker-mongodb "image badge") [![](https://images.microbadger.com/badges/version/tbaltrushaitis/docker-mongodb.svg)](https://microbadger.com/images/tbaltrushaitis/docker-mongodb "")

![MongoDB Logo](https://bitbucket.org/tbaltrushaitis/docker-mongodb/avatar/128)

`docker` `docker-mongodb` `dockerized-service`

--------

**Dockerized MongoDB** - Provide a Docker Container with MongoDB service preinstalled

--------

## Building the MongoDB Docker image

```dockerfile
# Format: docker build --tag/-t <user-name>/<repository> .
# Example:

$ docker -D build --no-cache --tag tbaltrushaitis/docker-mongodb:3.4.0 .
```

Once this command is issued, Docker will go through the `Dockerfile` and build
the image. The final image will be tagged `tbaltrushaitis/docker-mongodb:3.4.0`.

> **Tip:**  It is always a good practice to tag Docker images by passing the `--tag` option to `docker build` command.

------------------

## Pushing the MongoDB image to Docker Hub

To use the `docker push` command need to be logged-in.

### Log-in

```
$ docker -D login

Username: ..
```

### Push the image

**Format:**

    docker push <user-name>/<repository>

```
$ docker -D push tbaltrushaitis/docker-mongodb:3.4.0
The push refers to a repository [tbaltrushaitis/gsmc-public] (len: 1)
Sending image list
Pushing repository tbaltrushaitis/docker-mongodb (1 tags)
..
```

------------------

### More Info ###

 - [Linking containers](https://docs.docker.com/engine/userguide/networking/default_network/dockerlinks.md)
 - [Cross-host linking containers](https://docs.docker.com/engine/admin/ambassador_pattern_linking.md)
 - [Creating an Automated Build](https://docs.docker.com/docker-hub/builds/)
