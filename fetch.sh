#!/bin/bash

git checkout build.sh
git checkout fetch.sh
git checkout .env
git checkout Dockerfile
git checkout docker-compose.yml

git fetch
git pull
